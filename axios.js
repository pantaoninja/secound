/**
 * 
 * @param {String} url 
 * @param {String} params 
 * @param {Function} headersFn 
 * @returns Promise
 */


function get(url,params,headersFn=null){
    return new Promise((resolve,reject)=>{
        let xhr=new XMLHttpRequest
        xhr.onreadystatechange=function(){
        if(xhr.readyState==4){
        if(xhr.status==200){
              let res=JSON.parse(xhr.responseText)
           
              resolve(res)
        }
        else{
            reject(xhr.status)
        }
        }
        }
        xhr.open('get',`${url}?${params}`)
        if(headersFn) headersFn(xhr)
        xhr.send(null)

    }) 
    }




    function post(url,params,headersFn=null){
        return new Promise((resolve,reject)=>{
            let xhr=new XMLHttpRequest
            xhr.onreadystatechange=function(){
            if(xhr.readyState==4){
            if(xhr.status==200){
                  let res=JSON.parse(xhr.responseText)
               
                  resolve(res)
            }
            else{
                reject(xhr.status)
            }
            }
            }
            xhr.open('post',url)
            xhr.setRequestHeader('content-type','application/x-www-form-urlencoded')
            if(headersFn) headersFn(xhr)
            xhr.send(params)
    
        }) 
        }