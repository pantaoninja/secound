/**
 * @param {String} key
 * @param {String} val
 * @param {Number} time
 */
 function setcookie(key, val, time = 10) 
 {
     
     if (typeof val == 'object') val = JSON.stringify(val)
     const d = new Date   
     d.setTime(d.getTime() - 1000*60*60*8 + 1000*time)
     document.cookie = `${key}=${val}; expires=${d}`    
 }
 /**
  * @param {String} key
  * @returns String
  
  */
  function getcookie(key){
    let arr = document.cookie.split('; ')
    for (let i=0; i<arr.length; i++) 
    {
        let item = arr[i]
        let tempArr = item.split('=') 
        if (tempArr[0] == key) return tempArr[1]
    }
}


/**
 * @param {String} key
 * @returns undefined
 */
 function removecookie(key)
 {
     setcookie(key, null, -1) 
 }