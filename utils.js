/**
 * 获取参数
 * 
 */
function getparams(){
    let query=window.location.href
    query=(decodeURI(query)).split('?')[1]
    let vars=query.split('&')
  for(let i=0;i<vars.length;i++){
      let tempArr=vars[i].split('=')
      if(tempArr[0]=key) return tempArr[1]
  }
  return false
}










/**
 * 日期格式化
 * @author webopenfather
 * @param {String} param 日期字符串
 * @returns String
 */
function dateFormat(param) 
{
    if (param === undefined) {
        return;
    }
    let date = new Date(param);
    
    let now = new Date();
    let digit = parseInt((now.getTime() - date.getTime()) / 1000);
    
    let suffix = '秒前';

    if (digit < 60) {
        return '刚刚';
    } else {
        digit = parseInt(digit / 60);
        // console.log(digit);
        suffix = '分钟前';
        console.log(digit+suffix);
        if (digit >= 60) {
            digit = parseInt(digit / 60);
            suffix = '小时前';
            console.log(digit+suffix);
            if (digit >= 24) {
                digit = parseInt(digit / 24);
                suffix = '天前';
                console.log(digit+suffix);
                if (digit >= 30) {
                digit = parseInt(digit / 30);
                suffix = '个月前';
                console.log(digit+suffix);
                    if (digit >= 12) {
                        digit = parseInt(digit / 12);
                        suffix = '年前';
                        console.log(digit+suffix);
                    }
                }
            }
        }
    }
    return digit + suffix;
}  

/**
 * 判断浏览器内核
 * @author webopenfather
 * @returns Object
 */
function browserData() 
{
    var u = navigator.userAgent;
    return {
        trident: u.indexOf('Trident') > -1, //IE内核
        presto: u.indexOf('Presto') > -1, //opera内核
        webKit: u.indexOf('AppleWebKit') > -1, //苹果、谷歌内核
        gecko: u.indexOf('Firefox') > -1, //火狐内核Gecko
        mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
        ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios
        android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android
        iPhone: u.indexOf('iPhone') > -1 , //iPhone
        iPad: u.indexOf('iPad') > -1, //iPad
        webApp: u.indexOf('Safari') > -1 //Safari
    }
}


/**
 * 判断是否是移动端
 * @author webopenfather
 * @returns Boolean
 */
function isM()
{
    var browser = {
        versions: function() {
          var u = navigator.userAgent;
          return {
                  mobile: !!u.match(/AppleWebKit.*Mobile.*/), //是否为移动终端
                  ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios
                  android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, //android
                  iPhone: u.indexOf('iPhone') > -1 , //iPhone
                  iPad: u.indexOf('iPad') > -1, //iPad
                  webApp: u.indexOf('Safari') > -1 //Safari
              };
          }
      }
      if (browser.versions.mobile || browser.versions.ios || browser.versions.android || browser.versions.iPhone || browser.versions.iPad) {
        // alert('移动端');
        return true
      } else {
        // alert('PC')
        return false
      }
}

/**
 * 判断是否是微信浏览器
 * @author webopenfather
 * @returns Boolean
 */
function isWx()
{
    var ua = navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i)=="micromessenger") {
        return true;
    } else {
        return false;
    }
}