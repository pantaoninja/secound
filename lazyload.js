/**
 * 图片懒加载
 * @author webopenfather
 * @returns undefined
 */
function lazyload(){
    var temp1 = window.innerHeight || document.documentElement.clientHeight
    var temp2 = document.body.scrollTop ||document.documentElement.scrollTop
    var totalHeight = temp1 +temp2
    var imgsObj = document.querySelectorAll('img')
    imgsObj.forEach(function(item){
        if(item.offsetTop<totalHeight){
            item.src = item.getAttribute('src-real')
        }
    })
}
lazyload()
window.onscroll = lazyloadlazyload.js