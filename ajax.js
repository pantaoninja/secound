/**
 * 
 * @param {String} url 
 * @param {String} params 
 * @param {Function} callback 
 * @param {Function} headersFn 
 * @return undefined
 */

function get(url,params,callback,headersFn=null){
let xhr=new XMLHttpRequest
xhr.onreadystatechange=function(){
if(xhr.readyState==4){
if(xhr.status==200){
      let res=JSON.parse(xhr.responseText)
   
      callback(res)
}
else{
    console.log(xhr.status)
}
}
}
xhr.open('get',`${url}?${params}`)
if(headersFn) headersFn(xhr)
xhr.send(null)


}



function post(url,params,callback,headersFn=null){
    let xhr=new XMLHttpRequest
    xhr.onreadystatechange=function(){
    if(xhr.readyState==4){
    if(xhr.status==200){
          let res=JSON.parse(xhr.responseText)
       
          callback(res)
    }
    else{
        console.log(xhr.status)
    }
    }
    }
    xhr.open('post',url)
    xhr.setRequestHeader('content-type','application/x-www-form-urlencoded')
    if(headersFn) headersFn(xhr)
    xhr.send(params)
    
    
    }