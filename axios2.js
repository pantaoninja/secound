/**
 * Promise封装Ajax
 * @param {String} url 
 * @param {Object} params 
 * @param {Function} headersFn 
 * @param {String} dataType
 * @returns Promise
 */


 function get(url,params={},headersFn=null,dataType='json'){
    return new Promise((resolve,reject)=>{
        let xhr=new XMLHttpRequest
        xhr.onreadystatechange=function(){
        if(xhr.readyState==4){
        if(xhr.status==200){
            let res=xhr.responseText
            switch(dataType){
                case "json":
                  res=JSON.parse(res)
                  break;
                  default:
                      break;  
            }
           
              resolve(res)
        }
        else{
            reject(xhr.status)
        }
        }
        }
        let tempArr=[]
        for(let key in params){
                 tempArr.push[`${key}=${params[key]}`]
        }
        xhr.open('get',`${url}?${tempArr.join('&')}`)
        if(headersFn) headersFn(xhr)
        xhr.send(null)

    }) 
    }



/**
 * Promise封装Ajax
 * @param {String} url 
 * @param {Object} params 
 * @param {Function} headersFn 
 * @param {String} dataType
 * @returns Promise
 */

    function post(url,params={},headersFn=null,dataType='json'){
        return new Promise((resolve,reject)=>{
            let xhr=new XMLHttpRequest
            xhr.onreadystatechange=function(){
            if(xhr.readyState==4){
            if(xhr.status==200){
                let res=xhr.responseText
                switch(dataType){
                    case "json":
                      res=JSON.parse(res)
                      break;
                      default:
                          break;  
                }
               
                  resolve(res)
            }
            else{
                reject(xhr.status)
            }
            }
            }
            xhr.open('post',url)
            xhr.setRequestHeader('content-type','application/x-www-form-urlencoded')
            let tempArr=[]
            for(let key in params){
                     tempArr.push[`${key}=${params[key]}`]
            }
            xhr.open('get',`${url}?${tempArr.join('&')}`)
            xhr.send(params)
    
        }) 
        }