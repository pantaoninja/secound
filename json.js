//parse方法
/**
 * 
 * @param {String} javaData
 * @returns Object
 */
function parse(javaData){
javaData=javaData.replace(/{|}|"/g,'')
let javaDataObj={}
let javaDataArr=javaData.split(',')
javaDataArr.forEach(item=>{
    let temp=item.split(':')
    let key=temp[0]
    let value=temp[1]
    javaDataObj[key]=value
})
return javaDataObj;
}
//stringify方法
/**
 * 
 * @param {Object} webObj
 * @returns String
 */
function stringify(webObj){
let arr=[]
for(let key in webObj){
    let temp=`"${key}":"${webObj[key]}"`
    arr.push(temp)
}
return `{${arr.join(',')}}`
    }

