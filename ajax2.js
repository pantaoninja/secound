/**
 * 
 * @param {String} url 
 * @param {String} params 
 * @param {Function} callback 
 * @param {Object} headers
 * @param {String} dataType  
 * @return undefined
 */

 function get(url,params,callback,headers={},dataType='json'){
    let xhr=new XMLHttpRequest
    xhr.onreadystatechange=function(){
    if(xhr.readyState==4){
    if(xhr.status==200){
          let res=xhr.responseText
          switch(dataType){
              case 'json':
                res=JSON.parse(res)
                break;
                default:
                    break; 
          }
       
          callback(res)
    }
    else{
        console.log(xhr.status)
    }
    }
    }
    xhr.open('get',`${url}?${params}`)
    for(let key in headers){
        xhr.setRequestHeader(key,headers[key])
    }
    
    xhr.send(null)
    }

    
    
    /**
     * 
     * @param {String} url 
     * @param {String} params 
     * @param {Function} callback 
     * @param {Object} headers
     * @param {Function} dataType  
     * @return undefined
     */
    
    
    function post(url,params,callback,headers={},dataType='json'){
        let xhr=new XMLHttpRequest
        xhr.onreadystatechange=function(){
        if(xhr.readyState==4){
        if(xhr.status==200){
    
              let res=xhr.responseText
              switch(dataType){
                  case "json":
                    res=JSON.parse(res)
                    break;
                    default:
                        break;  
              }
           
              callback(res)
        }
        else{
            console.log(xhr.status)
        }
        }
        }
        xhr.open('post',url)
        xhr.setRequestHeader('content-type','application/x-www-form-urlencoded')
       for(let key in headers){
        xhr.setRequestHeader(key,headers[key])
       }
        xhr.send(params)
        
        
        }