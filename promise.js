/**
 * Promise原理
 * @param {function} Promise 
 */
class Promise{
    PromiseState='pending'
    PromiseResult=undefined
constructor(callback){ 
    callback(res=>{
        if(this.PromiseState!='pending') return
        this.PromiseState='success'
        this.PromiseResult=res
    },err=>{
       if(this.PromiseState!='pending') return
       this.PromiseState='error'
        this.PromiseResult=err
    })
}
}
Promise.prototype.then=function(){}
Promise.prototype.catch=function(){}
Promise.prototype.finnally=function(){}
Promise.resolve=function(){}
Promise.reject=function(){}
Promise.all=function(){}
Promise.race=function(){}
Promise.allSettled=function(){}
Promise.any=function(){}
